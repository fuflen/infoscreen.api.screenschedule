﻿using _2.InfoScreen.API.ScreenSchedule.Infrastruct.HostedServices;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.ConnectionHub;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.HostedServices;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Service;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Service.Interfaces;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Moq;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models.DTO;
using _4.InfoScreen.API.ScreenSchedule.Domain.Result;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.SignalR;
using FluentAssertions;

namespace InfoScreen.APi.ScreenSchedule.UnitTests.ScheduleQueueHostedServiceTests.RunSchedulesTests
{
    public class RunScheduleTests
    {
        public void Fixture()
        {

        }

        [Theory]
        [MemberData(nameof(RunSchedulesTestDataGenerator.ValidData_LoopAtleastOnce),
            MemberType = typeof(RunSchedulesTestDataGenerator))]
        public async Task RunSchedules_ValidMemberData_LoopAtleastOnce(InfoScreenQueueItem item, PopulatedViewDTO DTO)
        {
            ScheduleQueueHostedService service = GetScheduleQueueHostedService();

            Mock<ICommandViewHub> commandViewHubMock = GetMockedCommandViewHub(item, DTO);
            Mock<IViewService> viewServiceMock = GetMockedViewService(DTO);

            var source = new CancellationTokenSource();


            service.RunSchedules(item, viewServiceMock.Object, commandViewHubMock.Object, source.Token);

            await Task.Delay(500);
            commandViewHubMock.Verify(mock => mock.SendMessageToGroup(item.InfoScreenId.ToString(), DTO.ViewContent), Times.AtLeastOnce);
            source.Cancel();
        }

        [Theory]
        [MemberData(nameof(RunSchedulesTestDataGenerator.ValidData_ExpiredDataAndUpdatedList),
            MemberType = typeof(RunSchedulesTestDataGenerator))]
        public async Task RunSchedules_ValidMemberData_ExecuteSuccesfull(InfoScreenQueueItem item, PopulatedViewDTO DTO)
        {
            ScheduleQueueHostedService service = GetScheduleQueueHostedService();

            Mock<ICommandViewHub> commandViewHubMock = GetMockedCommandViewHub(item, DTO);
            Mock<IViewService> viewServiceMock = GetMockedViewService(DTO);

            var source = new CancellationTokenSource();


            var xyz = Task.Run(() => service.RunSchedules(item, viewServiceMock.Object, commandViewHubMock.Object, source.Token));
            await xyz;

            Assert.True(xyz.IsCompletedSuccessfully);
        }

        [Theory]
        [MemberData(nameof(RunSchedulesTestDataGenerator.ValidData_LongRunning),
            MemberType = typeof(RunSchedulesTestDataGenerator))]
        public async Task RunSchedules_ValidMemberData_TaskCanceledExceptionThrown(InfoScreenQueueItem item, PopulatedViewDTO DTO)
        {
            ScheduleQueueHostedService service = GetScheduleQueueHostedService();

            Mock<ICommandViewHub> commandViewHubMock = GetMockedCommandViewHub(item, DTO);
            Mock<IViewService> viewServiceMock = GetMockedViewService(DTO);


            // Instead of Action we use func - why?
            Func<Task> runSchedules = async () =>
            {
                var task = service.RunSchedules(item, viewServiceMock.Object, commandViewHubMock.Object, item.TokenSource.Token);
                await Task.Delay(1000);
                item.TokenSource.Cancel();
                task.Wait();
            };

            runSchedules.Should().Throw<AggregateException>().WithInnerExceptionExactly<TaskCanceledException>();

            // Old test with XUnit
            //var t = await Assert.ThrowsAsync<AggregateException>(async () => {
            //    var task = service.RunSchedules(item, viewServiceMock.Object, commandViewHubMock.Object, item.TokenSource.Token);
            //    await Task.Delay(1000);
            //    item.TokenSource.Cancel();
            //    task.Wait();
            //});
            //var x = t.InnerException.GetType().IsEquivalentTo(exception.GetType());
            //Assert.True(x);


        }

        [Theory]
        [MemberData(nameof(RunSchedulesTestDataGenerator.ValidData_LongRunning),
            MemberType = typeof(RunSchedulesTestDataGenerator))]
        public async Task RunSchedules_ValidMemberData_TaskCanceledExceptionThrown_Immediately(InfoScreenQueueItem item, PopulatedViewDTO DTO)
        {
            ScheduleQueueHostedService service = GetScheduleQueueHostedService();

            Mock<ICommandViewHub> commandViewHubMock = GetMockedCommandViewHub(item, DTO);
            Mock<IViewService> viewServiceMock = GetMockedViewService(DTO);

            Func<Task> runSchedules = async () =>
            {
                item.TokenSource.Cancel();
                var task = service.RunSchedules(item, viewServiceMock.Object, commandViewHubMock.Object, item.TokenSource.Token);
                await task;
            };

            runSchedules.Should().Throw<AggregateException>().WithInnerExceptionExactly<TaskCanceledException>();
        }



        private static Mock<IViewService> GetMockedViewService(PopulatedViewDTO DTO)
        {
            Mock<IViewService> viewServiceMock = new Mock<IViewService>();
            viewServiceMock.Setup(x => x.GetPopulatedView(
                It.Is<int>(i => i == DTO.Id)
                )).Returns(Task.FromResult(Result<PopulatedViewDTO>.Success(DTO)));
            return viewServiceMock;
        }

        private static Mock<ICommandViewHub> GetMockedCommandViewHub(InfoScreenQueueItem item, PopulatedViewDTO DTO)
        {
            Mock<ICommandViewHub> commandViewHubMock = new Mock<ICommandViewHub>();


            commandViewHubMock.Setup(x => x.SendMessageToGroup(
                It.Is<string>(i => i == item.InfoScreenId.ToString()),
                It.Is<string>(i => i == DTO.ViewContent)
                )).Returns(Task.Run(() => { }));
            return commandViewHubMock;
        }

        private static ScheduleQueueHostedService GetScheduleQueueHostedService()
        {
            IServiceCollection services = new ServiceCollection();
            services.AddSingleton<ILoggerFactory, NullLoggerFactory>();
            services.AddHostedService<ScheduleQueueHostedService>();
            var serviceProvider = services.BuildServiceProvider();
            var service = serviceProvider.GetService<IHostedService>() as ScheduleQueueHostedService;
            return service;
        }
    }
}
