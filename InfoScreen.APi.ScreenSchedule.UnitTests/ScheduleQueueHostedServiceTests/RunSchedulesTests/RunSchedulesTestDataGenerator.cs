﻿using _3.InfoScreen.API.ScreenSchedule.Infrastruct.HostedServices;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InfoScreen.APi.ScreenSchedule.UnitTests.ScheduleQueueHostedServiceTests.RunSchedulesTests
{
    public class RunSchedulesTestDataGenerator
    {

        public int InfoScreenId { get; set; }
        public CancellationTokenSource TokenSource { get; set; }
        public int NumberOfConnectedScreens { get; set; }
        public Queue<ScheduleModel> ScheduleQueue { get; set; }
        public Queue<ScheduleModel> UpdatedScheduleQueue { get; set; }

        static PopulatedViewDTO DTO = new PopulatedViewDTO()
        {
            Id = 1,
            ViewName = "TestView",
            ViewContent = "TestViewContent"
        };



        public static IEnumerable<object[]> ValidData_LoopAtleastOnce()
        {
            // Test case 2 configuration
            var queueItem = new InfoScreenQueueItem()
            {
                InfoScreenId = 1,
                TokenSource = new CancellationTokenSource(),
                NumberOfConnectedScreens = 1,
                ScheduleQueue = new Queue<ScheduleModel>()
            };
            
            queueItem.ScheduleQueue.Enqueue(new ScheduleModel()
            {
                Id = 1,
                ExpireDateTime = DateTime.Now.AddMinutes(10),
                InfoScreenId = 1,
                OnScreenTime = 1,
                StartDateTime = DateTime.Now,
                ViewId = 1,
                ViewName = "TestView"
            });

            // Test Case 2
            yield return new object[] { queueItem, DTO };
            
        }

        public static IEnumerable<object[]> ValidData_ExpiredDataAndUpdatedList()
        {
           
            // Test case 1 configuration
            var queueItem = new InfoScreenQueueItem()
            {
                InfoScreenId = 1,
                TokenSource = new CancellationTokenSource(),
                NumberOfConnectedScreens = 1,
                ScheduleQueue = new Queue<ScheduleModel>(),
                UpdatedScheduleQueue = new Queue<ScheduleModel>()
            };

            queueItem.ScheduleQueue.Enqueue(new ScheduleModel()
            {
                Id = 1,
                ExpireDateTime = DateTime.Now,
                InfoScreenId = 1,
                OnScreenTime = 1,
                StartDateTime = DateTime.Now,
                ViewId = 1,
                ViewName = "TestView"
            });

            yield return new object[] { queueItem, DTO };
            queueItem.ScheduleQueue = new Queue<ScheduleModel>();
            yield return new object[] { queueItem, DTO };
        }

        public static IEnumerable<object[]> ValidData_LongRunning()
        {

            // Test case 1 configuration
            var queueItem = new InfoScreenQueueItem()
            {
                InfoScreenId = 1,
                TokenSource = new CancellationTokenSource(),
                NumberOfConnectedScreens = 1,
                ScheduleQueue = new Queue<ScheduleModel>()
            };

            queueItem.ScheduleQueue.Enqueue(new ScheduleModel()
            {
                Id = 1,
                ExpireDateTime = DateTime.Now.AddMinutes(10),
                InfoScreenId = 1,
                OnScreenTime = 30,
                StartDateTime = DateTime.Now,
                ViewId = 1,
                ViewName = "TestView"
            });

            yield return new object[] { queueItem, DTO };
        }

        //public static IEnumerable<object[]> GetInvalidData()
        //{
        //    // Test Case 3
        //    yield return new object[] { InvalidDTO_NoFields, new ArgumentNullException() };
        //    // Test Case 1
        //    yield return new object[] { InvalidDTO_NoData, new ArgumentNullException() };
        //    // Test Case 2
        //    yield return new object[] { InvalidDTO_NullData, new ArgumentNullException() };
        //}
    }
}
