﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using _4.InfoScreen.API.ScreenSchedule.Domain.ValidationMessages;

namespace _2.InfoScreen.API.ScreenSchedule.Application.Validators.ScheduleValidators
{
    public class ScheduleOnScreenTimeValidator : AbstractValidator<int>
    {
        public ScheduleOnScreenTimeValidator()
        {
            RuleFor(x => x).
                NotNull().
                WithMessage(ErrorMessages.ScheduleOnScreenTimeNull).
                GreaterThan(0).
                WithMessage(ErrorMessages.ScheduleOnScreenTimeInvalid);
        }
    }
}
