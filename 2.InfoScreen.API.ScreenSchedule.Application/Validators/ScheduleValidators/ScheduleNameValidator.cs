﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using _4.InfoScreen.API.ScreenSchedule.Domain.ValidationMessages;

namespace _2.InfoScreen.API.ScreenSchedule.Application.Validators.ScheduleValidators
{
    public class ScheduleNameValidator : AbstractValidator<string>
    {
        public ScheduleNameValidator()
        {
            RuleFor(x => x).
                NotNull().
                WithMessage(ErrorMessages.ScheduleNameNull).
                NotEmpty().
                WithMessage(ErrorMessages.ScheduleNameEmpty).
                Must(BeValidNameLength).
                WithMessage(ErrorMessages.ScheduleNameLengthInvalid);
        }

        private static bool BeValidNameLength(string name)
        {
            return name != null && name.Length >= 1 && name.Length <= 50;
        }
    }
}
