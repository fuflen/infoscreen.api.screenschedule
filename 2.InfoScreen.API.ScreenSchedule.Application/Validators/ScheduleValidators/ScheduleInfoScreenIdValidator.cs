﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using _4.InfoScreen.API.ScreenSchedule.Domain.ValidationMessages;

namespace _2.InfoScreen.API.ScreenSchedule.Application.Validators.ScheduleValidators
{
    public class ScheduleInfoScreenIdValidator : AbstractValidator<int>
    {
        public ScheduleInfoScreenIdValidator()
        {
            RuleFor(x => x).
                NotNull().
                WithMessage(ErrorMessages.ScheduleInfoScreenIdNull).
                GreaterThan(0).
                WithMessage(ErrorMessages.ScheduleInfoScreenIdInvalid);
        }
    }
}
