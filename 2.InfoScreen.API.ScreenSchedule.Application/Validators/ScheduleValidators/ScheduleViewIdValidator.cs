﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using _4.InfoScreen.API.ScreenSchedule.Domain.ValidationMessages;

namespace _2.InfoScreen.API.ScreenSchedule.Application.Validators.ScheduleValidators
{
    class ScheduleViewIdValidator : AbstractValidator<int>
    {
        public ScheduleViewIdValidator()
        {
            RuleFor(x => x).
                NotNull().
                WithMessage(ErrorMessages.ScheduleViewIdNull).
                GreaterThan(0).
                WithMessage(ErrorMessages.ScheduleViewIdInvalid);
        }
    }
}
