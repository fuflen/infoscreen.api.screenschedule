﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using _4.InfoScreen.API.ScreenSchedule.Domain.ValidationMessages;

namespace _2.InfoScreen.API.ScreenSchedule.Application.Validators.InfoScreenValidators
{
    public class InfoScreenNameValidator : AbstractValidator<string>
    {
        public InfoScreenNameValidator()
        {
            RuleFor(x => x).
                NotNull().
                WithMessage(ErrorMessages.InfoScreenNameNull).
                NotEmpty().
                WithMessage(ErrorMessages.InfoScreenNameEmpty).
                Must(BeValidNameLength).
                WithMessage(ErrorMessages.InfoScreenNameLengthInvalid);
        }

        private static bool BeValidNameLength(string name)
        {
            return name != null && name.Length >= 1 && name.Length <= 50;
        }
    }
}
