﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using _4.InfoScreen.API.ScreenSchedule.Domain.ValidationMessages;

namespace _2.InfoScreen.API.ScreenSchedule.Application.Validators.InfoScreenValidators
{
    class InfoScreenIdValidator : AbstractValidator<int>
    {
        public InfoScreenIdValidator()
        {
            RuleFor(x => x).
                NotNull().
                WithMessage(ErrorMessages.InfoScreenIdNull).
                GreaterThan(0).
                WithMessage(ErrorMessages.InfoScreenIdInvalid);
        }
    }
}
