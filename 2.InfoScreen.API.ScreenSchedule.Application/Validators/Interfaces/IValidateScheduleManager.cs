﻿using System;
using System.Collections.Generic;
using System.Text;
using _2.InfoScreen.API.ScreenSchedule.Application.Requests.CRUD;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models.Interfaces;

namespace _2.InfoScreen.API.ScreenSchedule.Application.Validators.Interfaces
{
    public interface IValidateScheduleManager<T> where T : DTOInterface
    {
        void ValidateCreateRequest(CreateRequest<T> request);
        void ValidateUpdateRequest(UpdateRequest<T> request);
        void ValidateGetByIdRequest(GetByIdRequest request);
        void ValidateDeleteRequest(DeleteRequest request);
    }
}
