﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using _2.InfoScreen.API.ScreenSchedule.Application.Requests.CRUD;
using _2.InfoScreen.API.ScreenSchedule.Application.Requests.InfoScreenRequests;
using _2.InfoScreen.API.ScreenSchedule.Application.Validators.InfoScreenValidators;
using _2.InfoScreen.API.ScreenSchedule.Application.Validators.Interfaces;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models;

namespace _2.InfoScreen.API.ScreenSchedule.Application.Validators.Managers
{
    public class ValidateInfoScreenManager : IValidateInfoScreenManager<InfoScreenDTO>
    {
        private readonly IValidator<int> _idValidator;
        private readonly IValidator<string> _nameValidator;

        public ValidateInfoScreenManager()
        {
            _idValidator = new InfoScreenIdValidator();
            _nameValidator = new InfoScreenNameValidator();
        }
        public void ValidateCreateRequest(CreateRequest<InfoScreenDTO> request)
        {
            _nameValidator.ValidateAndThrow(request.dto.Name);
        }

        public void ValidateDeleteRequest(DeleteRequest request)
        {
            _idValidator.ValidateAndThrow(request.Id);
        }

        public void ValidateGetByIdRequest(GetByIdRequest request)
        {
            _idValidator.ValidateAndThrow(request.Id);
        }

        public void ValidateGetByIdWithScheduleListRequest(GetByIdWithScheduleListRequest request)
        {
            _idValidator.ValidateAndThrow(request.Id);
        }

        public void ValidateUpdateRequest(UpdateRequest<InfoScreenDTO> request)
        {
            _idValidator.ValidateAndThrow(request.dto.Id);
            _nameValidator.ValidateAndThrow(request.dto.Name);
        }
    }
}
