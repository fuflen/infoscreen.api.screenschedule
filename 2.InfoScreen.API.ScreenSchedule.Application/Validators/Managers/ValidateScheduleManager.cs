﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using _2.InfoScreen.API.ScreenSchedule.Application.Requests.CRUD;
using _2.InfoScreen.API.ScreenSchedule.Application.Requests.InfoScreenRequests;
using _2.InfoScreen.API.ScreenSchedule.Application.Validators.Interfaces;
using _2.InfoScreen.API.ScreenSchedule.Application.Validators.ScheduleValidators;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models.DTO;

namespace _2.InfoScreen.API.ScreenSchedule.Application.Validators.Managers
{
    public class ValidateScheduleManager : IValidateScheduleManager<ScheduleDTO>
    {
        private readonly IValidator<int> _idValidator;
        private readonly IValidator<int> _viewIdValidator;
        private readonly IValidator<int> _infoScreenIdValidator;
        private readonly IValidator<int> _onScreenTimeValidator;
        private readonly IValidator<string> _nameValidator;


        public ValidateScheduleManager()
        {
            _idValidator = new ScheduleIdValidator();
            _viewIdValidator = new ScheduleViewIdValidator();
            _nameValidator = new ScheduleNameValidator();
            _infoScreenIdValidator = new ScheduleInfoScreenIdValidator();
            _onScreenTimeValidator = new ScheduleOnScreenTimeValidator();

        }

        public void ValidateCreateRequest(CreateRequest<ScheduleDTO> request)
        {
            _viewIdValidator.ValidateAndThrow(request.dto.ViewId);
            _infoScreenIdValidator.ValidateAndThrow(request.dto.InfoScreenId);
            _onScreenTimeValidator.ValidateAndThrow(request.dto.OnScreenTime);
            _nameValidator.ValidateAndThrow(request.dto.ViewName);
        }

        public void ValidateDeleteRequest(DeleteRequest request)
        {
            _idValidator.ValidateAndThrow(request.Id);
        }

        public void ValidateGetByIdRequest(GetByIdRequest request)
        {
            _idValidator.ValidateAndThrow(request.Id);
        }

        public void ValidateGetByUrlRequest(GetByIdWithScheduleListRequest request)
        {
            _idValidator.ValidateAndThrow(request.Id);
        }

        public void ValidateUpdateRequest(UpdateRequest<ScheduleDTO> request)
        {
            _idValidator.ValidateAndThrow(request.dto.Id);
            _viewIdValidator.ValidateAndThrow(request.dto.ViewId);
            _infoScreenIdValidator.ValidateAndThrow(request.dto.InfoScreenId);
            _onScreenTimeValidator.ValidateAndThrow(request.dto.OnScreenTime);
            _nameValidator.ValidateAndThrow(request.dto.ViewName);
        }
    }
}
