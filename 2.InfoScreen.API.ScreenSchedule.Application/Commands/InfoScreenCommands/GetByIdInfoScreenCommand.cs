﻿using _2.InfoScreen.API.ScreenSchedule.Application.Requests.CRUD;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models;
using _4.InfoScreen.API.ScreenSchedule.Domain.Result;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.ScreenSchedule.Application.Commands.InfoScreenCommands
{
    public class GetByIdInfoScreenCommand : IRequest<Result<InfoScreenDTO>>
    {
        public GetByIdInfoScreenCommand(GetByIdRequest requestModel)
        {
            RequestModel = requestModel;
        }

        public GetByIdRequest RequestModel { get; set; }
    }
}
