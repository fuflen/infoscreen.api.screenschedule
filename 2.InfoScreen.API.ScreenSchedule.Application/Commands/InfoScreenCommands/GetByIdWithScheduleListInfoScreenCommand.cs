﻿using _2.InfoScreen.API.ScreenSchedule.Application.Requests.CRUD;
using _2.InfoScreen.API.ScreenSchedule.Application.Requests.InfoScreenRequests;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models;
using _4.InfoScreen.API.ScreenSchedule.Domain.Result;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.ScreenSchedule.Application.Commands.InfoScreenCommands
{
    public class GetByIdWithScheduleListInfoScreenCommand : IRequest<Result<InfoScreenDTO>>
    {
        public GetByIdWithScheduleListInfoScreenCommand(GetByIdWithScheduleListRequest requestModel)
        {
            RequestModel = requestModel;
        }

        public GetByIdWithScheduleListRequest RequestModel { get; set; }
    }
}
