﻿using _2.InfoScreen.API.ScreenSchedule.Application.Requests.CRUD;
using _2.InfoScreen.API.ScreenSchedule.Application.Requests.InfoScreenRequests;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models;
using _4.InfoScreen.API.ScreenSchedule.Domain.Result;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.ScreenSchedule.Application.Commands.InfoScreenCommands
{
    public class CreateInfoScreenCommand : IRequest<Result<InfoScreenDTO>>
    {
        public CreateInfoScreenCommand(CreateRequest<InfoScreenDTO> requestModel)
        {
            RequestModel = requestModel;
        }

        public CreateRequest<InfoScreenDTO> RequestModel { get; set; }
    }
}
