﻿using _2.InfoScreen.API.ScreenSchedule.Application.Requests.CRUD;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models;
using _4.InfoScreen.API.ScreenSchedule.Domain.Result;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.ScreenSchedule.Application.Commands.InfoScreenCommands
{
    public class DeleteInfoScreenCommand : IRequest<Result<InfoScreenDTO>>
    {
        public DeleteInfoScreenCommand(DeleteRequest requestModel)
        {
            RequestModel = requestModel;
        }

        public DeleteRequest RequestModel { get; set; }
    }
    
    
}
