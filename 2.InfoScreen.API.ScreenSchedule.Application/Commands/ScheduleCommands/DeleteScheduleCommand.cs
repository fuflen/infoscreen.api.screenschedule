﻿using _2.InfoScreen.API.ScreenSchedule.Application.Requests.CRUD;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models.DTO;
using _4.InfoScreen.API.ScreenSchedule.Domain.Result;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.ScreenSchedule.Application.Commands.ScheduleCommands
{
    public class DeleteScheduleCommand : IRequest<Result<ScheduleDTO>>
    {
        public DeleteScheduleCommand(DeleteRequest requestModel)
        {
            RequestModel = requestModel;
        }

        public DeleteRequest RequestModel { get; set; }
    }

}
