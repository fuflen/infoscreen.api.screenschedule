﻿using _2.InfoScreen.API.ScreenSchedule.Application.Requests.CRUD;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models.DTO;
using _4.InfoScreen.API.ScreenSchedule.Domain.Result;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.ScreenSchedule.Application.Commands.ScheduleCommands
{
    public class UpdateScheduleCommand : IRequest<Result<ScheduleDTO>>
    {
        public UpdateScheduleCommand(UpdateRequest<ScheduleDTO> requestModel)
        {
            RequestModel = requestModel;
        }

        public UpdateRequest<ScheduleDTO> RequestModel { get; set; }
    }
}
