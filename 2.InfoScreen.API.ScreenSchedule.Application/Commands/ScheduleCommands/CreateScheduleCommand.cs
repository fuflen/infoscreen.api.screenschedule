﻿using _2.InfoScreen.API.ScreenSchedule.Application.Requests.CRUD;
using _2.InfoScreen.API.ScreenSchedule.Application.Requests.InfoScreenRequests;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models.DTO;
using _4.InfoScreen.API.ScreenSchedule.Domain.Result;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.ScreenSchedule.Application.Commands.ScheduleCommands
{
    public class CreateScheduleCommand : IRequest<Result<ScheduleDTO>>
    {
        public CreateScheduleCommand(CreateRequest<ScheduleDTO> requestModel)
        {
            RequestModel = requestModel;
        }

        public CreateRequest<ScheduleDTO> RequestModel { get; set; }
    }
}
