﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.ScreenSchedule.Application.Requests.InfoScreenRequests
{
    public class GetByIdWithScheduleListRequest
    {
        public int Id { get; set; }
    }
}
