﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.ScreenSchedule.Application.Requests.CRUD
{
    public class DeleteRequest
    {
        public int Id { get; set; }
    }
}
