﻿using _4.InfoScreen.API.ScreenSchedule.Domain.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.ScreenSchedule.Application.Requests.CRUD
{
    public class CreateRequest<T> where T : DTOInterface
    {
        public T dto { get; set; }
    }
}
