﻿using _2.InfoScreen.API.ScreenSchedule.Application.Commands.ScheduleCommands;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Interfaces;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models.DTO;
using _4.InfoScreen.API.ScreenSchedule.Domain.Result;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using _2.InfoScreen.API.ScreenSchedule.Application.Validators.Interfaces;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.ConnectionHub;

namespace _2.InfoScreen.API.ScreenSchedule.Application.Handlers.ScheduleHandlers
{
    public class CreateScheduleHandler : IRequestHandler<CreateScheduleCommand, Result<ScheduleDTO>>
    {
        private IScheduleRepository _repo;
        private IMapper _mapper;
        private readonly IValidateScheduleManager<ScheduleDTO> _validateManager;
        private BackgroundServiceHub _serviceHub;
        //private ILogger _logger;


        public CreateScheduleHandler(
            IScheduleRepository repo, 
            IMapper mapper,
            IValidateScheduleManager<ScheduleDTO> validateManager,
            BackgroundServiceHub serviceHub)
        {
            _repo = repo;
            _mapper = mapper;
            _validateManager = validateManager;
            _serviceHub = serviceHub;
            //_logger = logger;
        }


        public async Task<Result<ScheduleDTO>> Handle(CreateScheduleCommand command, CancellationToken cancellationToken)
        {
            try
            {
                _validateManager.ValidateCreateRequest(command.RequestModel);

                var mapperConfig = new MapperConfiguration(
                    configuration =>
                    {
                        configuration.CreateMap<ScheduleDTO, ScheduleModel>()
                            .ForMember(x => x.InfoScreen, y => y.Ignore());
                    });

                _mapper = mapperConfig.CreateMapper();

                ScheduleModel model = _mapper.Map<ScheduleModel>(command.RequestModel.dto);

                var entity = await _repo.Create(model);
                ScheduleDTO dto = _mapper.Map<ScheduleDTO>(entity);

                _serviceHub.UpdateInfoScreenForClient(dto.InfoScreenId);

                return Result<ScheduleDTO>.Success(dto);
            }
            catch (Exception e)
            {
                //if (e is ValidationException)
                //{
                //    //_logger.LogError("Validation error with message: " + e.Message);
                //}

                return Result<ScheduleDTO>.Error(e);
            }
        }
    }
}
