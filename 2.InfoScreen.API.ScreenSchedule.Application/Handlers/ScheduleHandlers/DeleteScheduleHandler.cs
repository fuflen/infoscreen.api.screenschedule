﻿using _2.InfoScreen.API.ScreenSchedule.Application.Commands.ScheduleCommands;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Interfaces;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models.DTO;
using _4.InfoScreen.API.ScreenSchedule.Domain.Result;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using _2.InfoScreen.API.ScreenSchedule.Application.Validators.Interfaces;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.ConnectionHub;

namespace _2.InfoScreen.API.ScreenSchedule.Application.Handlers.ScheduleHandlers
{
    public class DeleteScheduleHandler : IRequestHandler<DeleteScheduleCommand, Result<ScheduleDTO>>
    {
        private IScheduleRepository _repo;
        private IMapper _mapper;
        private readonly IValidateScheduleManager<ScheduleDTO> _validateManager;
        private BackgroundServiceHub _serviceHub;
        //private ILogger _logger;


        public DeleteScheduleHandler(
            IScheduleRepository repo, 
            IMapper mapper,
            IValidateScheduleManager<ScheduleDTO> validateManager,
                BackgroundServiceHub serviceHub)
        {
            _repo = repo;
            _mapper = mapper;
            _validateManager = validateManager;
            _serviceHub = serviceHub;
            //_logger = logger;
        }


        public async Task<Result<ScheduleDTO>> Handle(DeleteScheduleCommand command, CancellationToken cancellationToken)
        {
            try
            {
                _validateManager.ValidateDeleteRequest(command.RequestModel);
                var entity = await _repo.Delete(command.RequestModel.Id);
                ScheduleDTO dto = _mapper.Map<ScheduleDTO>(entity);

                _serviceHub.UpdateInfoScreenForClient(dto.InfoScreenId);

                return Result<ScheduleDTO>.Success(dto);
            }
            catch (Exception e)
            {
                //if (e is ValidationException)
                //{
                //    //_logger.LogError("Validation error with message: " + e.Message);
                //}

                return Result<ScheduleDTO>.Error(e);
            }
        }
    }
}
