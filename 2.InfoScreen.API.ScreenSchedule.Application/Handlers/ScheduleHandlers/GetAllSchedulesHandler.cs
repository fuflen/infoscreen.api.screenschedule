﻿using _2.InfoScreen.API.ScreenSchedule.Application.Commands.ScheduleCommands;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Interfaces;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models.DTO;
using _4.InfoScreen.API.ScreenSchedule.Domain.Result;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using _2.InfoScreen.API.ScreenSchedule.Application.Validators.Interfaces;

namespace _2.InfoScreen.API.ScreenSchedule.Application.Handlers.ScheduleHandlers
{
    public class GetAllSchedulesHandler : IRequestHandler<GetAllSchedulesCommand, Result<List<ScheduleDTO>>>
    {
        private IScheduleRepository _repo;
        private IMapper _mapper;
        //private ILogger _logger;


        public GetAllSchedulesHandler(
            IScheduleRepository repo, 
            IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
            //_logger = logger;
        }


        public async Task<Result<List<ScheduleDTO>>> Handle(GetAllSchedulesCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var entity = await Task.Factory.StartNew(() => _repo.GetAll().ToList(), cancellationToken);
                List<ScheduleDTO> dto = _mapper.Map<List<ScheduleDTO>>(entity);
                return Result<List<ScheduleDTO>>.Success(dto);
            }
            catch (Exception e)
            {
                //if (e is ValidationException)
                //{
                //    //_logger.LogError("Validation error with message: " + e.Message);
                //}

                return Result<List<ScheduleDTO>>.Error(e);
            }
        }
    }
}
