﻿using _2.InfoScreen.API.ScreenSchedule.Application.Commands.InfoScreenCommands;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Interfaces;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models;
using _4.InfoScreen.API.ScreenSchedule.Domain.Result;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using _2.InfoScreen.API.ScreenSchedule.Application.Validators.Interfaces;

namespace _2.InfoScreen.API.ScreenSchedule.Application.Handlers.InfoScreenHandlers
{
    public class CreateInfoScreenHandler : IRequestHandler<CreateInfoScreenCommand, Result<InfoScreenDTO>>
    {
        private readonly IInfoScreenRepository _repo;
        private IMapper _mapper;
        private readonly IValidateInfoScreenManager<InfoScreenDTO> _validateManager;
        //private ILogger _logger;


        public CreateInfoScreenHandler(
            IInfoScreenRepository repo, 
            IMapper mapper,
            IValidateInfoScreenManager<InfoScreenDTO> validateManager)
        {
            _repo = repo;
            _mapper = mapper;
            _validateManager = validateManager;
            //_logger = logger;
        }


        public async Task<Result<InfoScreenDTO>> Handle(CreateInfoScreenCommand command, CancellationToken cancellationToken)
        {
            try
            {
                _validateManager.ValidateCreateRequest(command.RequestModel);

                var mapperConfig = new MapperConfiguration(
                    configuration =>
                    {
                        configuration.CreateMap<InfoScreenDTO, InfoScreenModel>()
                            .ForMember(x => x.ScheduleList, y => y.Ignore());
                    });

                _mapper = mapperConfig.CreateMapper();

                InfoScreenModel model = _mapper.Map<InfoScreenModel>(command.RequestModel.dto);

                var entity = await _repo.Create(model);
                InfoScreenDTO dto = _mapper.Map<InfoScreenDTO>(entity);
                return Result<InfoScreenDTO>.Success(dto);
            }
            catch (Exception e)
            {
                //if (e is ValidationException)
                //{
                //    //_logger.LogError("Validation error with message: " + e.Message);
                //}

                return Result<InfoScreenDTO>.Error(e);
            }
        }
    }
}
