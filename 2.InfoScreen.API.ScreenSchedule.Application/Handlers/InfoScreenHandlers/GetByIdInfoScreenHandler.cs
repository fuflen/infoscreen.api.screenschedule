﻿using _2.InfoScreen.API.ScreenSchedule.Application.Commands.InfoScreenCommands;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Interfaces;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models;
using _4.InfoScreen.API.ScreenSchedule.Domain.Result;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.SignalR;
using System.Threading;
using System.Threading.Tasks;
using _2.InfoScreen.API.ScreenSchedule.Application.Validators.Interfaces;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.ConnectionHub;

namespace _2.InfoScreen.API.ScreenSchedule.Application.Handlers.InfoScreenHandlers
{
    public class GetByIdInfoScreenHandler : IRequestHandler<GetByIdInfoScreenCommand, Result<InfoScreenDTO>>
    {
        private IInfoScreenRepository _repo;
        private IMapper _mapper;
        private readonly IValidateInfoScreenManager<InfoScreenDTO> _validateManager;
        //private ILogger _logger;


        public GetByIdInfoScreenHandler(
            IInfoScreenRepository repo,
            IMapper mapper,
            IValidateInfoScreenManager<InfoScreenDTO> validateManager)
        {
            _repo = repo;
            _mapper = mapper;
            _validateManager = validateManager;
            //_logger = logger;
        }


        public async Task<Result<InfoScreenDTO>> Handle(GetByIdInfoScreenCommand command, CancellationToken cancellationToken)
        {
            try
            {
                _validateManager.ValidateGetByIdRequest(command.RequestModel);



                var entity = await _repo.GetById(command.RequestModel.Id);
                InfoScreenDTO dto = _mapper.Map<InfoScreenDTO>(entity);

                return Result<InfoScreenDTO>.Success(dto);
            }
            catch (Exception e)
            {
                //if (e is ValidationException)
                //{
                //    //_logger.LogError("Validation error with message: " + e.Message);
                //}

                return Result<InfoScreenDTO>.Error(e);
            }
        }
    }
}
