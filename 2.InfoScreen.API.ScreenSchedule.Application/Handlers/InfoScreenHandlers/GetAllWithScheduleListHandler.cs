﻿using _2.InfoScreen.API.ScreenSchedule.Application.Commands.InfoScreenCommands;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Interfaces;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models;
using _4.InfoScreen.API.ScreenSchedule.Domain.Result;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _2.InfoScreen.API.ScreenSchedule.Application.Handlers.InfoScreenHandlers
{
    class GetAllWithScheduleListHandler : IRequestHandler<GetAllWithScheduleListCommand, Result<List<InfoScreenDTO>>>
    {
        private IInfoScreenRepository _repo;
        private IMapper _mapper;
        //private IValidateManager<DataEndpointDTO> _validateManager;
        //private ILogger _logger;


        public GetAllWithScheduleListHandler(
            IInfoScreenRepository repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
            //_validateManager = validateManager;
            //_logger = logger;
        }


        public async Task<Result<List<InfoScreenDTO>>> Handle(GetAllWithScheduleListCommand command, CancellationToken cancellationToken)
        {
            try
            {
                //_validateManager.ValidateCreateRequest(command.RequestModel);

                var entity = await Task.Factory.StartNew(() => _repo.GetAllWithScheduleList().ToList());
                List<InfoScreenDTO> dto = _mapper.Map<List<InfoScreenDTO>>(entity);
                return Result<List<InfoScreenDTO>>.Success(dto);
            }
            catch (Exception e)
            {
                //if (e is ValidationException)
                //{
                //    //_logger.LogError("Validation error with message: " + e.Message);
                //}

                return Result<List<InfoScreenDTO>>.Error(e);
            }
        }
    }
}
