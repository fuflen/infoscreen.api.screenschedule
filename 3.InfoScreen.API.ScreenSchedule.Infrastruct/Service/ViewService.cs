﻿using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Service.Interfaces;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models.DTO;
using _4.InfoScreen.API.ScreenSchedule.Domain.Result;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace _3.InfoScreen.API.ScreenSchedule.Infrastruct.Service
{
    public class ViewService : IViewService
    {
        private HttpClient _client;
        private string mappingServiceUrl = "http://viewservice/api/PopulatedView/GetPopulatedView";
        public ViewService(HttpClient client)
        {
            this._client = client;
        }


        public async Task<Result<PopulatedViewDTO>> GetPopulatedView(int viewId)
        {
            HttpResponseMessage response = await _client.GetAsync(mappingServiceUrl + "/" + viewId);

            var dto = await response.Content.ReadAsAsync<Result<PopulatedViewDTO>>();

            return dto;
        }
    }
}