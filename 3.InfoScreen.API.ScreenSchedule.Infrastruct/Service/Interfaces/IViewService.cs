﻿using _4.InfoScreen.API.ScreenSchedule.Domain.Models.DTO;
using _4.InfoScreen.API.ScreenSchedule.Domain.Result;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace _3.InfoScreen.API.ScreenSchedule.Infrastruct.Service.Interfaces
{
    public interface IViewService
    {
        Task<Result<PopulatedViewDTO>> GetPopulatedView(int viewId);
    }
}
