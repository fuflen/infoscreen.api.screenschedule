﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3.InfoScreen.API.ScreenSchedule.Infrastruct.Service.Interfaces
{
    public interface IMessagingGateway
    {
        void SendScheduleDeleteFailedEndpointMessage(int endpointId, List<int> viewIdd);
        void SendScheduleDeleteFailedTemplateMessage(int templateId, List<int> viewIdd);
        void SendScheduleDeleteFailedViewMessage(int endpointId, int templateId, int viewId);
    }
}
