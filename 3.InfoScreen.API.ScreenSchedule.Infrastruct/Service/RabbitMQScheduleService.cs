﻿using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Service.Interfaces;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models.RabbitMQModels;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models.RabbitMQModels.MessageDTO.OutgoingMessages.DeleteFailedMessages;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace _3.InfoScreen.API.ScreenSchedule.Infrastruct.Service
{
    public class RabbitMQScheduleService : IMessagingGateway
    {
        private RabbitMQConnectionModel _connectionModel;

        private const string ScheduleDeleteFailedEndpointRoutingKey = "schedule.delete.failed.endpoint";
        private const string ScheduleDeleteFailedTemplateRoutingKey = "schedule.delete.failed.template";
        private const string ScheduleDeleteFailedViewRoutingKey = "schedule.delete.failed.view";

        public RabbitMQScheduleService(RabbitMQConnectionService connectionService)
        {
            _connectionModel = connectionService.GetConnectionModel();
        }

        public void SendScheduleDeleteFailedEndpointMessage(int endpointId,List<int> viewIds)
        {
            var model = new ScheduleDeleteFailedEndpointMessage()
            {
                ViewIds = viewIds,
                EndpointId = endpointId
            };

            byte[] body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(model));

            SendMessage(body, ScheduleDeleteFailedEndpointRoutingKey);
        }

        public void SendScheduleDeleteFailedTemplateMessage(int templateId, List<int> viewIds)
        {
            var model = new ScheduleDeleteFailedTemplateMessage()
            {
                ViewIds = viewIds,
                TemplateId = templateId
            };

            byte[] body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(model));

            SendMessage(body, ScheduleDeleteFailedViewRoutingKey);
        }

        public void SendScheduleDeleteFailedViewMessage(int endpointId, int templateId, int viewId)
        {
            var model = new ScheduleDeleteFailedViewMessage()
            {
                ViewId = viewId,
                EndpointId = endpointId,
                TemplateId = templateId
            };

            byte[] body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(model));

            SendMessage(body, ScheduleDeleteFailedViewRoutingKey);
        }

        private void SendMessage(byte[] message, string routingKey)
        {
            _connectionModel.Model.BasicPublish(_connectionModel.ExchangeName, routingKey, _connectionModel.Properties, message);
        }
    }
}
