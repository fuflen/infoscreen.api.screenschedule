﻿using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Context;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Interfaces;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace _3.InfoScreen.API.ScreenSchedule.Infrastruct.Repositories
{
    public class ScheduleRepository : GenericRepository<ScheduleModel>, IScheduleRepository
    {
        private ScheduleContext _dbContext;
        public ScheduleRepository(ScheduleContext dbContext)
        :base(dbContext)
        {
            _dbContext = dbContext;
            dbContext.Database.EnsureCreated();
        }

        private async Task<List<ScheduleModel>> GetManyByViewIdList(List<int> ids)
        {
            return await _dbContext.Set<ScheduleModel>().Where(x => ids.Contains( x.ViewId) ).ToListAsync();
        }
        private async Task<List<ScheduleModel>> GetManyByViewId(int viewId)
        {
            return await _dbContext.Set<ScheduleModel>().Where(x => x.ViewId == viewId).ToListAsync();
        }

        public async Task<List<ScheduleModel>> DeleteManyByViewId(int viewId)
        {
            var entitiesToDelete = await GetManyByViewId(viewId);
            entitiesToDelete.ForEach(x => x.Deleted = true);
            _dbContext.Set<ScheduleModel>().UpdateRange(entitiesToDelete);
            await _dbContext.SaveChangesAsync();
            return entitiesToDelete;
        }

        public async Task<List<ScheduleModel>> UndoDeleteManyByViewId(int viewId)
        {
            var entitiesToRemove = await GetManyByViewId(viewId);
            entitiesToRemove.ForEach(x => x.Deleted = false);
            _dbContext.Set<ScheduleModel>().UpdateRange(entitiesToRemove);
            await _dbContext.SaveChangesAsync();
            return entitiesToRemove;
        }

        public async Task<List<ScheduleModel>> DeleteManyByViewIdList(List<int> viewIds)
        {
            var entitiesToDelete = await GetManyByViewIdList(viewIds);
            entitiesToDelete.ForEach(x => x.Deleted = true);
            _dbContext.Set<ScheduleModel>().UpdateRange(entitiesToDelete);
            await _dbContext.SaveChangesAsync();
            return entitiesToDelete;
        }

        public async Task<List<ScheduleModel>> UndoDeleteManyByViewIdList(List<int> viewIds)
        {
            var entitiesToRemove = await GetManyByViewIdList(viewIds);
            entitiesToRemove.ForEach(x => x.Deleted = false);
            _dbContext.Set<ScheduleModel>().UpdateRange(entitiesToRemove);
            await _dbContext.SaveChangesAsync();
            return entitiesToRemove;
        }
    }
}
