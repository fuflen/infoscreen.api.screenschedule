﻿using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Context;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Interfaces;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3.InfoScreen.API.ScreenSchedule.Infrastruct.Repositories
{
    public class InfoScreenRepository: GenericRepository<InfoScreenModel>, IInfoScreenRepository
    {
        private ScheduleContext _dbContext;

        public InfoScreenRepository(ScheduleContext dbContext)
            :base(dbContext)
        {
            dbContext.Database.EnsureCreated();
            _dbContext = dbContext;
        }

        public async Task<InfoScreenModel> GetInfoScreenWithScheduleList(int id)
        {
            var xyz = await _dbContext.Set<InfoScreenModel>().Where(x => !x.Deleted).Include(x => x.ScheduleList).Where(s => !s.Deleted)
                          .FirstAsync(e => e.Id == id);

            return xyz;
        }

        /// <summary>
        /// Get the whole DbSet for the Entity.
        /// AsNoTracking as it improved performance. And Eliminated Reference loops. 
        /// </summary>
        /// <returns>IQuerable of <see cref="TEntity"/> so they can be further queued </returns>
        public IQueryable<InfoScreenModel> GetAllWithScheduleList()
        {
            return _dbContext.Set<InfoScreenModel>().Where(x => !x.Deleted).Include(x => x.ScheduleList).Where(s => !s.Deleted).AsNoTracking();
        }
    }
}
