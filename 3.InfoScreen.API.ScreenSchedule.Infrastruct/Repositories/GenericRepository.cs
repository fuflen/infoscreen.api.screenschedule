﻿using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Context;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Interfaces;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3.InfoScreen.API.ScreenSchedule.Infrastruct.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity>
    where TEntity : class, IEntity
    {
        private readonly ScheduleContext _dbContext;

        /// <summary>
        /// The constructor for the Generic Repository
        /// </summary>
        /// <param name="dbContext">The context to use. </param>
        public GenericRepository(ScheduleContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Saved the given Entity to its corresponding DbSet table. 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<TEntity> Create(TEntity entity)
        {
            entity.Deleted = false;
            await _dbContext.Set<TEntity>().AddAsync(entity);
            await _dbContext.SaveChangesAsync();
            return entity;
        }

        /// <summary>
        /// Deleted the Entity with the given id. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<TEntity> Delete(int id)
        {
            var entity = await GetById(id);

            entity.Deleted = true;

            var updatedEntity = _dbContext.Set<TEntity>().Update(entity).Entity;

            await _dbContext.SaveChangesAsync();

            return updatedEntity;
        }

        /// <summary>
        /// Get the whole DbSet for the Entity.
        /// AsNoTracking as it improved performance. And Eliminated Reference loops. 
        /// </summary>
        /// <returns>IQuerable of <see cref="TEntity"/> so they can be further queued </returns>
        public IQueryable<TEntity> GetAll()
        {
            return _dbContext.Set<TEntity>().Where(x => !x.Deleted).AsNoTracking();
        }

        /// <summary>
        /// Gets the Entity with the given id. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<TEntity> GetById(int id)
        {
            return await _dbContext.Set<TEntity>()
                            .AsNoTracking()
                            .FirstAsync(e => e.Id == id && !e.Deleted);
        }

        /// <summary>
        /// Updates the entity wíth the given id.
        /// </summary>
        /// <param name="entity">The updated Entity</param>
        /// <returns></returns>
        public async Task<TEntity> Update(TEntity entity)
        {
            var oldEntity = await GetById(entity.Id);
            entity.Deleted = oldEntity.Deleted;

            var updatedEntity = _dbContext.Set<TEntity>().Update(entity).Entity;
            await _dbContext.SaveChangesAsync();

            return updatedEntity;
        }
    }
}
