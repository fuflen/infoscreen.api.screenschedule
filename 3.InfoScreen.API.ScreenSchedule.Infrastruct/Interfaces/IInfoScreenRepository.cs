﻿using _4.InfoScreen.API.ScreenSchedule.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3.InfoScreen.API.ScreenSchedule.Infrastruct.Interfaces
{
    public interface IInfoScreenRepository : IGenericRepository<InfoScreenModel>
    {
        Task<InfoScreenModel> GetInfoScreenWithScheduleList(int id);
        IQueryable<InfoScreenModel> GetAllWithScheduleList();
    }
}
