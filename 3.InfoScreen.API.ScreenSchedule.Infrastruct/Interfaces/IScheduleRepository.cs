﻿using _4.InfoScreen.API.ScreenSchedule.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace _3.InfoScreen.API.ScreenSchedule.Infrastruct.Interfaces
{
    public interface IScheduleRepository : IGenericRepository<ScheduleModel>
    {
        Task<List<ScheduleModel>> DeleteManyByViewId(int viewId);
        Task<List<ScheduleModel>> UndoDeleteManyByViewId(int viewId);
        Task<List<ScheduleModel>> DeleteManyByViewIdList(List<int> viewIds);
        Task<List<ScheduleModel>> UndoDeleteManyByViewIdList(List<int> viewIds);
    }
}
