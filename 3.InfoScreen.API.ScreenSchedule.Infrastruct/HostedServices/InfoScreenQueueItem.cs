﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models.DTO;

namespace _3.InfoScreen.API.ScreenSchedule.Infrastruct.HostedServices
{
    public class InfoScreenQueueItem
    {
        public int InfoScreenId { get; set; }
        public CancellationTokenSource TokenSource { get; set; }
        public int NumberOfConnectedScreens { get; set; }
        public Queue<ScheduleModel> ScheduleQueue { get; set; }
        public Queue<ScheduleModel> UpdatedScheduleQueue { get; set; }

    }
}
