﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.DependencyInjection;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.ConnectionHub;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.HostedServices;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Interfaces;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Service.Interfaces;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models;

namespace _2.InfoScreen.API.ScreenSchedule.Infrastruct.HostedServices
{
    public class ScheduleQueueHostedService : IHostedService
    {
        HubConnection _connection;
        private IViewService _viewService;
        private IScheduleRepository _scheduleRepo;
        private ICommandViewHub _viewHub;
        public IServiceProvider Services { get; }


        private static List<InfoScreenQueueItem> screenQueueItems = new List<InfoScreenQueueItem>();

        public ScheduleQueueHostedService(IServiceProvider services)
        {
            Services = services;
            _connection = new HubConnectionBuilder()
                .WithUrl("http://screenscheduleservice/BackgroundServiceHub")

                .Build();


        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            InitMessageListeners(cancellationToken);

            while (true)
            {
                try
                {
                    await _connection.StartAsync(cancellationToken);

                    break;
                }
                catch
                {
                    await Task.Delay(1000);
                }
            }
        }

        private void InitMessageListeners(CancellationToken cancellationToken)
        {
            _connection.Closed += async error =>
            {
                await Task.Delay(5000);
                await _connection.StartAsync(cancellationToken);
            };

            _connection.On<string>("RemoveInfoScreen", (message) =>
            {
                int infoScreenId;

                int.TryParse(message, out infoScreenId);
                RemoveFromInfoScreenQueue(infoScreenId);
            });

            _connection.On<string>("UpdateInfoScreen", (message) =>
            {
                int infoScreenId;

                int.TryParse(message, out infoScreenId);
                UpdateInfoScreenQueue(infoScreenId);
            });

            _connection.On<string>("AddInfoScreen", async (message) =>
            {
                CancellationTokenSource cts = new CancellationTokenSource();
                var linkedCts = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken, cts.Token);

                int infoScreenId;

                int.TryParse(message, out infoScreenId);

                var screenItem = screenQueueItems.FirstOrDefault(x => x.InfoScreenId == infoScreenId);
                if (screenItem != null)
                {
                    screenItem.NumberOfConnectedScreens += 1;
                }
                else
                {
                    await InitSchedule(infoScreenId, cts, linkedCts);
                }
            });
        }

        private void UpdateInfoScreenQueue(int infoScreenId)
        {
            var queueItem = screenQueueItems.FirstOrDefault(x => x.InfoScreenId == infoScreenId);
            if (queueItem != null)
            {
                using (var scope = Services.CreateScope())
                {
                    var scheduleRepo =
                        scope.ServiceProvider
                            .GetRequiredService<IScheduleRepository>();

                    queueItem.UpdatedScheduleQueue = new Queue<ScheduleModel>(scheduleRepo.GetAll()
                        .Where(x => x.InfoScreenId == queueItem.InfoScreenId).ToList());
                }
            }
        }

        private async Task InitSchedule(int infoScreenId, CancellationTokenSource cts,
            CancellationTokenSource linkedCts)
        {
            using (var scope = Services.CreateScope())
            {
                _viewService =
                    scope.ServiceProvider
                        .GetRequiredService<IViewService>();

                _scheduleRepo =
                    scope.ServiceProvider
                        .GetRequiredService<IScheduleRepository>();

                _viewHub =
                    scope.ServiceProvider
                        .GetRequiredService<CommandViewHub>();


                var queueItem = new InfoScreenQueueItem()
                {
                    InfoScreenId = infoScreenId,
                    TokenSource = cts,
                    NumberOfConnectedScreens = 1,
                    ScheduleQueue = new Queue<ScheduleModel>()
                };


                queueItem.ScheduleQueue = new Queue<ScheduleModel>(_scheduleRepo.GetAll()
                    .Where(x => x.InfoScreenId == queueItem.InfoScreenId).ToList());

                if (queueItem.ScheduleQueue.Any())
                {
                    screenQueueItems.Add(queueItem);

                    await RunInfoScreenTask(queueItem, linkedCts, cts);
                }
                else
                {
                    await _viewHub.SendMessageToGroup(queueItem.InfoScreenId.ToString(),
                        "<h1>No data to be displayed!</h1>");
                }
            }
        }

        private async Task RunInfoScreenTask(InfoScreenQueueItem queueItem, CancellationTokenSource linkedCts,
            CancellationTokenSource cts)
        {
            try
            {
                var t = Task.Run(async () => await RunSchedules(queueItem, _viewService, _viewHub, linkedCts.Token),
                        linkedCts.Token)
                    .ContinueWith((tsk, obj) =>
                    {
                        Debug.WriteLine("HEERRREE I AAAAAMMMMMM! BUT WITHOUT EXCEPTION");

                        tsk.Exception?.Flatten().Handle(ex =>
                        {
                            Debug.WriteLine("We have an Exception sir.");
                            return true;
                        });
                    }, TaskContinuationOptions.OnlyOnFaulted);

                await Task.WhenAny(t);
                screenQueueItems.RemoveAll(x => x.InfoScreenId == queueItem.InfoScreenId);
            }
            catch (Exception ex)
            {
                var x = ex.Message;
                cts.Cancel();
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return _connection.DisposeAsync();
        }

        public async Task RunSchedules(
            InfoScreenQueueItem queueItem,
            IViewService viewService,
            ICommandViewHub viewHub,
            CancellationToken linkedCt)
        {
            if (queueItem.ScheduleQueue.Any())
            {
                while (queueItem.ScheduleQueue.Any() && !linkedCt.IsCancellationRequested)
                {
                    var s = new ScheduleModel();
                    queueItem.ScheduleQueue.TryDequeue(out s);

                    var now = DateTime.Now;
                    if (s.StartDateTime < now && s.ExpireDateTime > now)
                    {
                        await Task.Delay(s.OnScreenTime * 100, linkedCt);

                        var view = await viewService.GetPopulatedView(s.ViewId);

                        await viewHub.SendMessageToGroup(queueItem.InfoScreenId.ToString(),
                            view.Data.ViewContent);

                    }
                    queueItem.ScheduleQueue.Enqueue(s);

                    if (queueItem.UpdatedScheduleQueue != null)
                    {
                        queueItem.ScheduleQueue = queueItem.UpdatedScheduleQueue;
                        queueItem.UpdatedScheduleQueue = null;
                    }

                }
                linkedCt.ThrowIfCancellationRequested();
            }

        }

        public void RemoveFromInfoScreenQueue(int infoScreenId)
        {

            var screenItem = screenQueueItems.FirstOrDefault(x => x.InfoScreenId == infoScreenId);
            if (screenItem != null)
            {
                if (screenItem.NumberOfConnectedScreens > 1)
                {
                    screenItem.NumberOfConnectedScreens -= 1;
                }
                else
                {
                    screenItem.TokenSource.Cancel();
                }
            }
        }
    }
}