﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Interfaces;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Service;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models.RabbitMQModels;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models.RabbitMQModels.MessageDTO;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models.RabbitMQModels.MessageDTO.OutgoingMessages.DeleteFailedMessages;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Service.Interfaces;

namespace _3.InfoScreen.API.ScreenSchedule.Infrastruct.HostedServices
{
    public class RabbitMQHostedService : IHostedService
    {
        private IServiceProvider _services;
        private RabbitMQConnectionModel _connectionModel;


        private const string UndoDeleteQueue = "delete_schedules";

        private const string ScheduleDeleteFailedEndpointBindingKey = "view.deleted.endpoint";
        private const string ScheduleDeleteFailedTemplateBindingKey = "view.deleted.template";
        private const string ScheduleDeleteFailedViewBindingKey = "mapping.deleted.view";

        public RabbitMQHostedService(RabbitMQConnectionService connection, IServiceProvider services)
        {
            Console.WriteLine("HOSTED SERVICE STARTING!!!");
            _services = services;
            _connectionModel = connection.GetConnectionModel();
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {


            _connectionModel.Model.QueueDeclare(UndoDeleteQueue, true, false, false, null);

            _connectionModel.Model.QueueBind(UndoDeleteQueue, _connectionModel.ExchangeName, ScheduleDeleteFailedEndpointBindingKey);
            _connectionModel.Model.QueueBind(UndoDeleteQueue, _connectionModel.ExchangeName, ScheduleDeleteFailedTemplateBindingKey);
            _connectionModel.Model.QueueBind(UndoDeleteQueue, _connectionModel.ExchangeName, ScheduleDeleteFailedViewBindingKey);

            var consumer = new EventingBasicConsumer(_connectionModel.Model);

            consumer.Received += async (model, ea) =>
            {
                await HandleReceivedEvent(ea);
            };


            _connectionModel.Model.BasicConsume(queue: UndoDeleteQueue,
                autoAck: true,
                consumer: consumer);
        }
        private async Task HandleReceivedEvent(BasicDeliverEventArgs ea)
        {
            Console.WriteLine("Handler started");

            switch (ea.RoutingKey)
            {
                case ScheduleDeleteFailedEndpointBindingKey:
                    ScheduleDeleteFailedEndpointMessage endpointMessage = Deserialize<ScheduleDeleteFailedEndpointMessage>(ea.Body);
                    await DeleteScheduleWithEndpoint(endpointMessage.EndpointId, endpointMessage.ViewIds);
                    break;
                case ScheduleDeleteFailedTemplateBindingKey:
                    ScheduleDeleteFailedTemplateMessage templateMessage = Deserialize<ScheduleDeleteFailedTemplateMessage>(ea.Body);
                    Console.WriteLine(templateMessage.ToString());
                    await DeleteScheduleWithTemplate(templateMessage.TemplateId, templateMessage.ViewIds);
                    break;
                case ScheduleDeleteFailedViewBindingKey:
                    ScheduleDeleteFailedViewMessage viewMessage = Deserialize<ScheduleDeleteFailedViewMessage>(ea.Body);
                    Console.WriteLine(viewMessage.ToString());
                    await DeleteScheduleWithView(viewMessage.EndpointId, viewMessage.TemplateId, viewMessage.ViewId);
                    break;
            }

        }



        public static T Deserialize<T>(byte[] data) where T : class
        {
            using (var stream = new MemoryStream(data))
            using (var reader = new StreamReader(stream, Encoding.UTF8))
                return JsonSerializer.Create().Deserialize(reader, typeof(T)) as T;
        }

        private async Task DeleteScheduleWithEndpoint(int endpointId, List<int> viewIds)
        {
            Console.WriteLine("Deleting schedules With View: " + endpointId);
            using (var scope = _services.CreateScope())
            {
                var scheduleRepo =
                    scope.ServiceProvider
                        .GetRequiredService<IScheduleRepository>();
                var messagingGateway =
                    scope.ServiceProvider
                        .GetRequiredService<IMessagingGateway>();

                try
                {
                    await scheduleRepo.DeleteManyByViewIdList(viewIds);
                }
                catch (Exception e)
                {
                    messagingGateway.SendScheduleDeleteFailedEndpointMessage(endpointId, viewIds);
                    Console.WriteLine("Error: " + e.Message);
                }
            }
        }

        private async Task DeleteScheduleWithTemplate(int templateId, List<int> viewIds)
        {
            Console.WriteLine("Deleting schedules With View: " + templateId);
            using (var scope = _services.CreateScope())
            {
                var scheduleRepo =
                    scope.ServiceProvider
                        .GetRequiredService<IScheduleRepository>();
                var messagingGateway =
                    scope.ServiceProvider
                        .GetRequiredService<IMessagingGateway>();

                try
                {
                    await scheduleRepo.DeleteManyByViewIdList(viewIds);

                    
                }
                catch (Exception e)
                {
                    messagingGateway.SendScheduleDeleteFailedTemplateMessage(templateId, viewIds);
                    Console.WriteLine("Error: " + e.Message);
                }
            }
        }

        private async Task DeleteScheduleWithView(int endpointId, int templateId, int viewId)
        {
            Console.WriteLine("Deleting schedules With View: " + viewId);
            using (var scope = _services.CreateScope())
            {
                var scheduleRepo =
                    scope.ServiceProvider
                        .GetRequiredService<IScheduleRepository>();
                var messagingGateway =
                    scope.ServiceProvider
                        .GetRequiredService<IMessagingGateway>();

                try
                {
                    await scheduleRepo.DeleteManyByViewId(viewId);

                    
                }
                catch (Exception e)
                {
                    messagingGateway.SendScheduleDeleteFailedViewMessage(endpointId, templateId, viewId);
                    Console.WriteLine("Error: " + e.Message);
                }
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                _connectionModel.Model.Dispose();
                cancellationToken.ThrowIfCancellationRequested();
            });
        }
    }
}
