﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace _3.InfoScreen.API.ScreenSchedule.Infrastruct.ConnectionHub
{
    public class CommandViewHub : ICommandViewHub
    {
        private readonly IHubContext<ViewHub> _hubContext;

        public CommandViewHub(IHubContext<ViewHub> hubContext)
        {
            _hubContext = hubContext;
        }


        public async Task SendMessageToGroup(string group, string message)
        {
            try
            {
                await _hubContext.Clients.Group(group).SendAsync("ShowView", message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }


}
