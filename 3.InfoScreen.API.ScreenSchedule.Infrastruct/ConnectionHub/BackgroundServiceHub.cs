﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.SignalR;

namespace _3.InfoScreen.API.ScreenSchedule.Infrastruct.ConnectionHub
{
    public class BackgroundServiceHub : Hub
    {
        private readonly IHubContext<BackgroundServiceHub> _hubContext;

        public BackgroundServiceHub(IHubContext<BackgroundServiceHub> hubContext)
        {
            _hubContext = hubContext;
        }
        public async void SendInfoScreenIdToClient(int infoScreenId)
        {
            await _hubContext.Clients.All.SendAsync("AddInfoScreen", infoScreenId.ToString());
        }


        

        public async void RemoveInfoScreenFromClient(int infoScreenId)
        {
            await _hubContext.Clients.All.SendAsync("RemoveInfoScreen", infoScreenId.ToString());
        }

        public async void UpdateInfoScreenForClient(int infoScreenId)
        {
            await _hubContext.Clients.All.SendAsync("UpdateInfoScreen", infoScreenId.ToString());
        }
    }
}
