﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace _3.InfoScreen.API.ScreenSchedule.Infrastruct.ConnectionHub
{
    public interface ICommandViewHub
    {
        Task SendMessageToGroup(string group, string message);
    }
}
