﻿using _3.InfoScreen.API.ScreenSchedule.Infrastruct.HostedServices;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Interfaces;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Service.Interfaces;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace _3.InfoScreen.API.ScreenSchedule.Infrastruct.ConnectionHub
{
    public class ViewHub : Hub
    {
        private readonly IHubContext<ViewHub> _hubContext;

        private IViewService _viewService;
        private IScheduleRepository _scheduleRepo;

        private BackgroundServiceHub _serviceHub;

        private List<InfoScreenQueueItem> screenQueueItems = new List<InfoScreenQueueItem>();

        public ViewHub(IHubContext<ViewHub> hubContext, IViewService viewService, IScheduleRepository scheduelRepository,
            BackgroundServiceHub serviceHub)
        {
            _hubContext = hubContext;
            _viewService = viewService;
            _scheduleRepo = scheduelRepository;
            _serviceHub = serviceHub;
        }

        public void ShowView(string message)
        {
            Console.WriteLine(message);
        }
        
        public async Task NewInfoScreen(int infoScreenId)
        {
            try
            {
                await _hubContext.Groups.AddToGroupAsync(Context.ConnectionId, infoScreenId.ToString());
                

                _serviceHub.SendInfoScreenIdToClient(infoScreenId);
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public async Task RemoveInfoScreen(int infoScreenId)
        {
            try
            {
                await _hubContext.Groups.RemoveFromGroupAsync(Context.ConnectionId, infoScreenId.ToString());

                _serviceHub.RemoveInfoScreenFromClient(infoScreenId);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
        
}
