﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models;

namespace _3.InfoScreen.API.ScreenSchedule.Infrastruct.Context
{
    public class DbInitializer : IDbInitializer
    {
        public void Initialize(ScheduleContext context)
        {
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            if (context.Schedule.Any() && context.ScreenModel.Any())
            {
                return;
            }

            List<InfoScreenModel> InfoScreenList = new List<InfoScreenModel>
            {
                new InfoScreenModel{ Name = "Sales Department" },
                new InfoScreenModel{ Name = "Development Department" },
                new InfoScreenModel{ Name = "Hallway" },
                new InfoScreenModel{ Name = "Cafeteria" },
                new InfoScreenModel{ Name = "Director" },
                new InfoScreenModel{ Name = "Sales Department" }
            };

            List<ScheduleModel> ScheduleList = new List<ScheduleModel>
            {
                new ScheduleModel{ ViewId = 1, ViewName = "Card View - Startwars People", OnScreenTime = 30, InfoScreenId = 1, StartDateTime = DateTime.Now, ExpireDateTime = DateTime.Now.AddMinutes(1) },
                new ScheduleModel{ ViewId = 2, ViewName = "Card View - Starwars Planets", OnScreenTime = 30, InfoScreenId = 1, StartDateTime = DateTime.Now, ExpireDateTime = DateTime.Now.AddMinutes(10) },
                new ScheduleModel{ ViewId = 3, ViewName = "Card View - Random Endpoint Data", OnScreenTime = 30, InfoScreenId = 3, StartDateTime = DateTime.Now, ExpireDateTime = DateTime.Now.AddMinutes(1) },
                new ScheduleModel{ ViewId = 5, ViewName = "Card View - Starwars Species", OnScreenTime = 30, InfoScreenId = 3, StartDateTime = DateTime.Now, ExpireDateTime = DateTime.Now.AddMinutes(1) }
            };

            context.ScreenModel.AddRange(InfoScreenList);
            context.Schedule.AddRange(ScheduleList);
            context.SaveChanges();
        }
    }
}
