﻿using _4.InfoScreen.API.ScreenSchedule.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace _3.InfoScreen.API.ScreenSchedule.Infrastruct.Context
{
    public class ScheduleContext : DbContext
    {
        public ScheduleContext(DbContextOptions<ScheduleContext> options)
            : base(options)
        {

        }

        public DbSet<ScheduleModel> Schedule { get; set; }
        public DbSet<InfoScreenModel> ScreenModel { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<ScheduleModel>(entity =>
            {
                entity.HasKey(x => x.Id);
                entity.Property(x => x.Id).ForSqlServerUseSequenceHiLo();
                entity.Property(x => x.ViewId).IsRequired();
                entity.Property(x => x.ViewName).IsRequired();
                entity.Property(x => x.OnScreenTime).IsRequired();
                entity.Property(x => x.StartDateTime);
                entity.Property(x => x.ExpireDateTime);
            });

            builder.Entity<InfoScreenModel>(entity =>
            {
                entity.HasKey(x => x.Id);
                entity.Property(x => x.Id).ForSqlServerUseSequenceHiLo();
                entity.Property(x => x.Name).IsRequired();
            });

            builder.Entity<ScheduleModel>().HasOne(x => x.InfoScreen).WithMany(x => x.ScheduleList).HasForeignKey(x => x.InfoScreenId);

           
            base.OnModelCreating(builder);
        }
    }
}
