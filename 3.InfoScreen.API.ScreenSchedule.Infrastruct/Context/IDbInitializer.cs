﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3.InfoScreen.API.ScreenSchedule.Infrastruct.Context
{
    public interface IDbInitializer
    {
        void Initialize(ScheduleContext context);
    }
}
