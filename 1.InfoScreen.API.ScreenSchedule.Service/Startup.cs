﻿using System;
using System.Net;
using _2.InfoScreen.API.ScreenSchedule.Application;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.ConnectionHub;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Context;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Interfaces;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Repositories;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Service;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.Service.Interfaces;
using AutoMapper;
using HealthChecks.UI.Client;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Newtonsoft.Json;
using StackExchange.Redis;
using Swashbuckle.AspNetCore.Swagger;
using _2.InfoScreen.API.ScreenSchedule.Application.CustomHealthChecks;
using _2.InfoScreen.API.ScreenSchedule.Application.Validators.Interfaces;
using _2.InfoScreen.API.ScreenSchedule.Application.Validators.Managers;
using _2.InfoScreen.API.ScreenSchedule.Infrastruct.HostedServices;
using _3.InfoScreen.API.ScreenSchedule.Infrastruct.HostedServices;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models.DTO;

namespace _1.InfoScreen.API.ScreenSchedule.Service
{
    public class Startup
    {
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public Startup(IConfiguration configuration)
        {
            IConfigurationRoot configRoot = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("AppSettings.Json", true, false)
                .AddEnvironmentVariables()
                .Build();

            Configuration = configRoot;
        }

        public IConfiguration Configuration { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2).AddJsonOptions(options => {
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });

            // Enable CORS from out frontend
            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                builder =>
                {
                    builder.WithOrigins("http://localhost:4200", "http://localhost:4201").AllowAnyHeader().AllowAnyMethod().AllowCredentials();
                });
            });


            services.AddTransient<IInfoScreenRepository, InfoScreenRepository>();
            services.AddTransient<IScheduleRepository, ScheduleRepository>();
            services.AddScoped<ICommandViewHub, CommandViewHub>();
            services.AddScoped<BackgroundServiceHub>();

            services.AddTransient<IMessagingGateway, RabbitMQScheduleService>();

            services.AddTransient<IDbInitializer, DbInitializer>();

            services.AddDbContext<ScheduleContext>(options =>
                options.UseSqlServer(Configuration["Connectionstring"])
            );

            services.AddHealthChecks()
                .AddCheck<MemoryHealthCheck>("Memory_check")
                .AddUrlGroup(uri =>
                    {
                        uri.AddUri(new Uri("http://viewservice/swagger"));
                        uri.ExpectHttpCode((int)HttpStatusCode.OK);
                        uri.UseGet();
                    }, "viewServiceConnection_check", HealthStatus.Unhealthy)
                .AddRabbitMQ(@"amqp://guest:guest@rabbitmq:5672", null, "rabbitMQConnection_check", HealthStatus.Unhealthy)
                .AddSqlServer(Configuration["Connectionstring"], "SELECT 1;", "SqlServerConnection_check", HealthStatus.Unhealthy)
                .AddSignalRHub(() => new HubConnectionBuilder()
                    .WithUrl("http://screenscheduleservice/BackgroundServiceHub")
                    .Build(), "signalRHubConnection_check", HealthStatus.Unhealthy);

            services.AddMediatR(typeof(Assembly));

            services.AddAutoMapper(typeof(Assembly));
            
            services.AddTransient<IValidateInfoScreenManager<InfoScreenDTO>, ValidateInfoScreenManager>();
            services.AddTransient<IValidateScheduleManager<ScheduleDTO>, ValidateScheduleManager>();
            services.AddSignalR(options =>
            {
                options.EnableDetailedErrors = true;
                options.KeepAliveInterval = TimeSpan.FromSeconds(3);
            });

            services.AddSingleton<RabbitMQConnectionService>();

            services.AddHostedService<RabbitMQHostedService>();

            services.AddHostedService<ScheduleQueueHostedService>();


            //Register the Endpoint service in the ServiceCollection.
            services.AddHttpClient<IViewService, ViewService>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Title = "ScreenSchedule API",
                    Description = "An API for managing screens and schedules",
                    Version = "v1"
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            using (var scope = app.ApplicationServices.CreateScope())
            {
                // Initialize the database
                var services = scope.ServiceProvider;
                var dbContext = services.GetService<ScheduleContext>();
                var dbInitializer = services.GetService<IDbInitializer>();
                dbInitializer.Initialize(dbContext);
            }

            app.UseCors(MyAllowSpecificOrigins);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "ScreenSchedule API v1");
            });

            app.UseHealthChecks("/health", new HealthCheckOptions
            {
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse,
                Predicate = registration => true
            });

            app.UseSignalR(route =>
            {
                route.MapHub<ViewHub>("/viewhub");
                route.MapHub<BackgroundServiceHub>("/backgroundServiceHub");
            });
            
            app.UseMvc();
        }
    }
}
