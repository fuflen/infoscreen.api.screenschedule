﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _2.InfoScreen.API.ScreenSchedule.Application.Commands.InfoScreenCommands;
using _2.InfoScreen.API.ScreenSchedule.Application.Requests.CRUD;
using _2.InfoScreen.API.ScreenSchedule.Application.Requests.InfoScreenRequests;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models;
using _4.InfoScreen.API.ScreenSchedule.Domain.Result;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace _1.InfoScreen.API.ScreenSchedule.Service.Controllers
{
    [Route("api/InfoScreen")]
    public class InfoScreenController : Controller
    {
        private IMediator _mediator;
        public InfoScreenController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [Route("GetAll")]
        public async Task<ActionResult<Result<List<InfoScreenDTO>>>> GetAll()
        {
            Result<List<InfoScreenDTO>> response = await _mediator.Send(new GetAllInfoScreenCommand(new GetAllRequest()));
            return Ok(response);
        }

        [HttpGet]
        [Route("GetAllWithScheduleList")]
        public async Task<ActionResult<Result<List<InfoScreenDTO>>>> GetAllWithScheduleList()
        {
            Result<List<InfoScreenDTO>> response = await _mediator.Send(new GetAllWithScheduleListCommand(new GetAllRequest()));
            return Ok(response);
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public async Task<ActionResult<Result<InfoScreenDTO>>> GetById([FromRoute]int id)
        {

            var response = await _mediator.Send(new GetByIdInfoScreenCommand(new GetByIdRequest() { Id = id }));
            return Ok(response);
        }

        [HttpPost]
        [Route("Create")]
        public async Task<ActionResult<Result<InfoScreenDTO>>> Create([FromBody]InfoScreenDTO endpoint)
        {

            Result<InfoScreenDTO> response =
                await _mediator.Send(
                    new CreateInfoScreenCommand(
                        new CreateRequest<InfoScreenDTO>() { dto = endpoint }));

            return Ok(response);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<ActionResult<Result<InfoScreenDTO>>> Delete([FromRoute] int id)
        {
            Result<InfoScreenDTO> response = await _mediator.Send(new DeleteInfoScreenCommand(new DeleteRequest { Id = id }));
            return Ok(response);
        }

        [HttpPut]
        [Route("Update")]
        public async Task<ActionResult<Result<InfoScreenDTO>>> Update([FromBody] InfoScreenDTO endpoint)
        {
            Result<InfoScreenDTO> response = await _mediator.Send(new UpdateInfoScreenCommand(new UpdateRequest<InfoScreenDTO> { dto = endpoint }));
            return Ok(response);
        }

        [HttpGet]
        [Route("GetByIdWithScheduleList/{id}")]
        public async Task<ActionResult<Result<InfoScreenDTO>>> GetByIdWithScheduleList([FromRoute]int id)
        {

            var response = await _mediator.Send(new GetByIdWithScheduleListInfoScreenCommand(new GetByIdWithScheduleListRequest() { Id = id }));

            return Ok(response);
        }
    }
}