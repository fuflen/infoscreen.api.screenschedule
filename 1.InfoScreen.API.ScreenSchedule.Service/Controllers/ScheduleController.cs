﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using _2.InfoScreen.API.ScreenSchedule.Application.Commands.ScheduleCommands;
using _2.InfoScreen.API.ScreenSchedule.Application.Requests.CRUD;

using _4.InfoScreen.API.ScreenSchedule.Domain.Models.DTO;
using _4.InfoScreen.API.ScreenSchedule.Domain.Result;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace _1.InfoScreen.API.ScreenSchedule.Service.Controllers
{
    [Route("api/Schedule")]
    public class ScheduleController : Controller
    {
        private IMediator _mediator;
        public ScheduleController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [Route("GetAll")]
        public async Task<ActionResult<Result<List<ScheduleDTO>>>> GetAll()
        {
            Result<List<ScheduleDTO>> response = await _mediator.Send(new GetAllSchedulesCommand(new GetAllRequest()));
            return Ok(response);
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public async Task<ActionResult<Result<ScheduleDTO>>> GetById([FromRoute]int id)
        {

            var response = await _mediator.Send(new GetByIdScheduleCommand(new GetByIdRequest() { Id = id }));
            return Ok(response);
        }

        [HttpPost]
        [Route("Create")]
        public async Task<ActionResult<Result<ScheduleDTO>>> Create([FromBody]ScheduleDTO endpoint)
        {

            Result<ScheduleDTO> response =
                await _mediator.Send(
                    new CreateScheduleCommand(
                        new CreateRequest<ScheduleDTO>() { dto = endpoint }));

            return Ok(response);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<ActionResult<Result<ScheduleDTO>>> Delete([FromRoute] int id)
        {
            Result<ScheduleDTO> response = await _mediator.Send(new DeleteScheduleCommand(new DeleteRequest { Id = id }));
            return Ok(response);
        }

        [HttpPut]
        [Route("Update")]
        public async Task<ActionResult<Result<ScheduleDTO>>> Update([FromBody] ScheduleDTO endpoint)
        {
            Result<ScheduleDTO> response = await _mediator.Send(new UpdateScheduleCommand(new UpdateRequest<ScheduleDTO> { dto = endpoint }));
            return Ok(response);
        }
    }
}