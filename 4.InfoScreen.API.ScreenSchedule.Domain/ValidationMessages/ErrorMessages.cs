﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.ScreenSchedule.Domain.ValidationMessages
{
    public class ErrorMessages
    {

        // Schedule name
        public const string ScheduleNameNull = "The Schedule Name is required and cannot be null.";
        public const string ScheduleNameEmpty = "Please enter a name for the Schedule.";
        public const string ScheduleNameLengthInvalid = "The Schedule name must be between 1 and 50 characters.";

        // Schedule id
        public const string ScheduleIdNull = "The ScheduleDTO id cannot be null.";
        public const string ScheduleIdInvalid = "The ScheduleDTO id needs to be higher than 0";

        // Schedule View id
        public const string ScheduleViewIdNull = "The ScheduleDTO View id cannot be null.";
        public const string ScheduleViewIdInvalid = "The ScheduleDTO View id needs to be higher than 0";

        // Schedule On screen time
        public const string ScheduleOnScreenTimeNull = "The ScheduleDTO View id cannot be null.";
        public const string ScheduleOnScreenTimeInvalid = "The ScheduleDTO View id needs to be higher than 0";

        // Schedule infoscreen id
        public const string ScheduleInfoScreenIdNull = "The ScheduleDTO View id cannot be null.";
        public const string ScheduleInfoScreenIdInvalid = "The ScheduleDTO View id needs to be higher than 0";

        // InfoScreen id
        public const string InfoScreenIdNull = "The InfoScreenDTO id cannot be null.";
        public const string InfoScreenIdInvalid = "The InfoScreenDTO id needs to be higher than 0";

        // InfoScreen name
        public const string InfoScreenNameNull = "The InfoScreen Name is required and cannot be null.";
        public const string InfoScreenNameEmpty = "Please enter a name for the InfoScreen.";
        public const string InfoScreenNameLengthInvalid = "The InfoScreen name must be between 1 and 50 characters.";
    }
}
