﻿using _4.InfoScreen.API.ScreenSchedule.Domain.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.ScreenSchedule.Domain.Models
{
    public class InfoScreenModel : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<ScheduleModel> ScheduleList { get; set; }
        public bool Deleted { get; set; }
    }
}
