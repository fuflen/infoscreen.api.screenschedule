﻿using _4.InfoScreen.API.ScreenSchedule.Domain.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using _4.InfoScreen.API.ScreenSchedule.Domain.Models.DTO;

namespace _4.InfoScreen.API.ScreenSchedule.Domain.Models
{
    public class InfoScreenDTO : DTOInterface
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<ScheduleDTO> ScheduleList { get; set; }
    }
}
