﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.ScreenSchedule.Domain.Models.DTO
{
    public class PopulatedViewDTO
    {
        public int Id { get; set; }
        public string ViewName { get; set; }
        public string ViewContent { get; set; }
    }
}
