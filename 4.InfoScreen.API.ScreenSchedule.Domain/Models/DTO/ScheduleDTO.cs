﻿using _4.InfoScreen.API.ScreenSchedule.Domain.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.ScreenSchedule.Domain.Models.DTO
{
    public class ScheduleDTO : DTOInterface
    {
        public int Id { get; set; }
        public int ViewId { get; set; }
        public string ViewName { get; set; }
        public int OnScreenTime { get; set; }
        public int InfoScreenId { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? ExpireDateTime { get; set; }
    }
}
