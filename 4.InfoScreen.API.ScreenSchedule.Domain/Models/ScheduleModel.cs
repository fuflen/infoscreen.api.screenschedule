﻿using _4.InfoScreen.API.ScreenSchedule.Domain.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.ScreenSchedule.Domain.Models
{
    public class ScheduleModel : IEntity
    {
        public int Id { get; set; }
        public int ViewId { get; set; }
        public string ViewName { get; set; }
        public int OnScreenTime { get; set; }
        public int InfoScreenId { get; set; }
        public bool Deleted { get; set; }

        public DateTime StartDateTime { get; set; }
        public DateTime ExpireDateTime { get; set; }
        public virtual InfoScreenModel InfoScreen { get; set; }
    }
}
