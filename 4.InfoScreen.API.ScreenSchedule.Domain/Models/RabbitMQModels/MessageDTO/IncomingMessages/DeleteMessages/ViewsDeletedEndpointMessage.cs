﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.ScreenSchedule.Domain.Models.RabbitMQModels.MessageDTO.IncomingMessages.DeleteMessages
{
    public class ViewsDeletedEndpointMessage
    {
        public List<int> ViewIds { get; set; }
        public int EndpointId { get; set; }
    }
}
