﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.ScreenSchedule.Domain.Models.RabbitMQModels.MessageDTO.IncomingMessages.DeleteMessages
{
    public class MappingDeletedViewMessage
    {
        public int ViewId { get; set; }
        public int EndpointId { get; set; }
        public int TemplateId { get; set; }
    }
}
