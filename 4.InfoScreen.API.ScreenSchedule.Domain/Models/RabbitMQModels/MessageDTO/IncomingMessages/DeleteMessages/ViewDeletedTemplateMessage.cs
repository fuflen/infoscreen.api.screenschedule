﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.ScreenSchedule.Domain.Models.RabbitMQModels.MessageDTO.IncomingMessages.DeleteMessages
{
    public class ViewDeletedTemplateMessage
    {
        public List<int> ViewIds { get; set; }
        public int TemplateId { get; set; }
    }
}
