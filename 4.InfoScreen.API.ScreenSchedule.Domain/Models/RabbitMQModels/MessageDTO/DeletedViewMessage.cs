﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.ScreenSchedule.Domain.Models.RabbitMQModels.MessageDTO
{
    public class DeletedViewMessage
    {
        public int ViewId { get; set; }
    }
}
